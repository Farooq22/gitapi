class MergeRequestsController < ApplicationController

  def create
    git_status = Git::Push.call(Git.open('./', log: Logger.new(STDOUT)))
    if git_status
      Gitlab::MergeRequest.call('master', 'master', 23141130, 23144558, 'merge request to child')

    end
  end
end
