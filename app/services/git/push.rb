class Git::Push < ApplicationService
  attr_reader :git

  def initialize(git)
    @git = git
  end

  def call
    begin
      git.push
      puts "ok"
      true
    rescue 
      puts 'no'
      false
    end
  end

end