class Gitlab::MergeRequest < ApplicationService
  attr_reader :source_branch, :target_branch, :source_project_id, :target_project_id, :title

  def initialize(source_branch, target_branch, source_project_id, target_project_id, title)
    @source_branch = source_branch
    @target_branch = target_branch
    @source_project_id = source_project_id
    @target_project_id = target_project_id
    @title = title
  end

  def call
    begin
      Gitlab.create_merge_request(source_project_id,
                                  title, options = { source_branch: source_branch,
                                                     target_branch: target_branch,
                                                     target_project_id: target_project_id })
      puts "ok"
      true
    rescue
      puts "ok"
      false
    end
  end
end