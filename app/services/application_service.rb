# frozen_string_literal: true

# Base Service Object class
class ApplicationService
  def self.call(*args, &block)
    new(*args, &block).call
  end

end