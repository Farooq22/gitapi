Rails.application.routes.draw do
  resources :merge_requests, only: [:create]

  root 'welcome#index'
end
